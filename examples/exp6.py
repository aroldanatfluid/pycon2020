from fluidasserts.proto import http
from . import get_bwapp_cookies

def test_sql_injection(vuln_ip):
    """App vulnerable a SQLi?."""
    bwapp_cookie = get_bwapp_cookies(vuln_ip)
    bwapp_cookie['security_level'] = '0'
    vulnerable_url = 'http://' + vuln_ip + '/sqli_1.php'
    params = {'title': 'a\'', 'action': 'search'}
    return http.has_sqli(vulnerable_url, params, cookies=bwapp_cookie)


if __name__ == '__main__':
    test_sql_injection('127.0.0.1')
